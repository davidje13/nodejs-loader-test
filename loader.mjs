import process from 'process';
import fs from 'fs';

import staticImport from './loader-imported.mjs';

const NODE = process.versions.node.split('.').map(Number);
const compareVersion = (v1, v2) => v1.map((v1part, i) => (v1part - (v2[i] || 0))).filter((diff) => diff)[0] || 0;

export async function load(url, context, defaultLoad) {
  if (url.includes('loader-imported.mjs')) {
    const raw = fs.readFileSync(new URL(url).pathname, 'utf-8');
    return { format: 'module', source: raw.replace(/not processed/g, 'processed') };
  }
  if (url.includes('imported.mjs')) {
    const { default: dynamicImport } = await import ('./loader-imported.mjs');
    const raw = fs.readFileSync(new URL(url).pathname, 'utf-8');
    return { format: 'module', source: raw.replace(/preprocessing-result-here/g, 'processed with static: ' + staticImport + ', dynamic: ' + dynamicImport) };
  }
  return defaultLoad(url, context, defaultLoad);
}

export const getSource = (compareVersion(NODE, [16, 12, 0]) < 0) && load;
