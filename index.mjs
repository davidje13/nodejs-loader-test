import process from 'process';

import a from './imported.mjs';
console.log(a);

if (a !== '<imported: processed with static: <loader-imported: not processed>, dynamic: <loader-imported: not processed>>') {
  process.exit(1);
}
